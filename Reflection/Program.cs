﻿using System.Text.Json;

namespace Reflection
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var mySerializer = new MySerializer<TestObject>();
            var obj = new TestObject();

            TestMySerializer(mySerializer, obj);

            
            Console.Write("Please enter iteration: ");
            long _iteration = 0;
            if (long.TryParse(Console.ReadLine(), out _iteration))
            {
                CompareSpeedSerialization(mySerializer, obj, _iteration);
            }
        }
        static void TestMySerializer(MySerializer<TestObject> mySerializer, TestObject obj)
        {
            var csv = mySerializer.Serialize(obj);
            Console.WriteLine(csv);
            var objectFromCSV = mySerializer.DeSerialize(csv);
            Console.WriteLine(objectFromCSV.ToString());

            var csv2 = "i5,i4,i3,i2,i1\n555,444,333,222,111";
            var objectFromCSV2 = mySerializer.DeSerialize(csv2);
            Console.WriteLine(objectFromCSV2.ToString());
        }

        static void CompareSpeedSerialization(MySerializer<TestObject> mySerializer, TestObject obj, long _iteration)
        {
            string mySerializerResult="";
            string systemSerializerResult = "";
            
            var startTime = DateTime.Now;
            for (long i = 0; i < _iteration; i++)
            {
                mySerializerResult=mySerializer.Serialize(obj);
            }
            var timer = DateTime.Now - startTime;
            startTime=DateTime.Now;
            Console.WriteLine("Time " + _iteration + " operation serialize (MySerializer) - " + timer.ToString());
            timer = DateTime.Now - startTime;
            Console.WriteLine("Time for write on Console - " + timer.ToString());

            startTime = DateTime.Now;
            for (long i = 0; i < _iteration; i++)
            {
                systemSerializerResult = JsonSerializer.Serialize(obj);
            }
            timer = DateTime.Now - startTime;
            Console.WriteLine("Time " + _iteration + " operation serialize (System JSON) - " + timer.ToString());

            startTime = DateTime.Now;
            for (long i = 0; i < _iteration; i++)
            {
                obj = mySerializer.DeSerialize(mySerializerResult);
            }
            timer = DateTime.Now - startTime;
            Console.WriteLine("Time " + _iteration + " operation deserialize (MySerializer) - " + timer.ToString());

            startTime = DateTime.Now;
            for (long i = 0; i < _iteration; i++)
            {
                obj = (TestObject)JsonSerializer.Deserialize(systemSerializerResult,typeof(TestObject));
            }
            timer = DateTime.Now - startTime;
            Console.WriteLine("Time " + _iteration + " operation deserialize (System JSON) - " + timer.ToString());

        }
    }
}