﻿namespace Reflection
{
    internal class TestObject
    {
        private int i1;
        public int i2;
        protected int i3;
        int i4;
        int i5;

        public TestObject() { 
            i1 = 1;
            i2 = 2;
            i3 = 3;
            i4 = 4;
            i5 = 5;
        }
        public override string ToString()
        {
            return "i1 = " + i1.ToString() + 
                ", i2 = " + i2.ToString() + 
                ", i3 = " + i3.ToString() + 
                ", i4 = " + i4.ToString() + 
                ", i5 = " + i5.ToString();
        }
    }
}
