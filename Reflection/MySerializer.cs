﻿using System.Reflection;
using System.ComponentModel;

namespace Reflection
{
    internal class MySerializer<T> where T : class, new()
    {
        private readonly BindingFlags allFlags;
        private List<FieldInfo> _fields;

        public MySerializer() 
        {
            long flags = 0;
            for (int i = 0; i < 25; i++)
            {
                flags += (long)Math.Pow(2, (double)i);
            }
            allFlags = (BindingFlags)flags;

            var type = typeof(T);
            this._fields = type.GetFields(allFlags).ToList();
        }

        public string Serialize(T obj)
        {
            var type = typeof(T);

            string[] resultNames = new string[_fields.Count];
            string[] resultValues = new string[_fields.Count];

            for (int i = 0; i < _fields.Count; i++)
            {
                resultNames[i] = _fields[i].Name;
                resultValues[i] = type.GetField(name: _fields[i].Name, bindingAttr: allFlags).GetValue(obj).ToString();
            }

            return string.Join(',', resultNames) + '\n' + string.Join(',', resultValues);
        }

        public T DeSerialize(string csv)
        {
            var result=new T();
            string[] csvArr=csv.Split('\n');
            string[] names = csvArr[0].Split(',');
            string[] values = csvArr[1].Split(',');

            for (int i = 0; i < _fields.Count; i++)
            {
                var value = values[i];
                var name = names[i];
                var field = _fields.First(f => f.Name == name);

                var converter = TypeDescriptor.GetConverter(_fields[0].FieldType);
                var convertedvalue = converter.ConvertFrom(value);

                field.SetValue(result, convertedvalue);
            }
            
            return result;
        }
    }
}
